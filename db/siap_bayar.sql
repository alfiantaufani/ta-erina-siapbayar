-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 30 Jun 2021 pada 10.52
-- Versi server: 10.4.19-MariaDB
-- Versi PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siap_bayar`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_siswa`
--

CREATE TABLE `data_siswa` (
  `id_siswa` int(11) NOT NULL,
  `nik` bigint(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(225) DEFAULT NULL,
  `nama_siswa` varchar(128) NOT NULL,
  `jenis_kelamin` varchar(128) NOT NULL,
  `paket_kelas_id` int(11) NOT NULL,
  `nama_ayah` varchar(128) NOT NULL,
  `nama_ibu` varchar(128) NOT NULL,
  `no_hp_ortu` varchar(20) DEFAULT NULL,
  `alamat_ortu` varchar(258) NOT NULL,
  `img_siswa` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_siswa`
--

INSERT INTO `data_siswa` (`id_siswa`, `nik`, `email`, `password`, `nama_siswa`, `jenis_kelamin`, `paket_kelas_id`, `nama_ayah`, `nama_ibu`, `no_hp_ortu`, `alamat_ortu`, `img_siswa`) VALUES
(128, 545435, 'erina1@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Erina 1', 'Perempuan', 1, 'Malcolm Hess', 'Alice Mercer', NULL, 'Esse iure fugiat co', NULL),
(129, 78, 'erina2@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Erina 2', 'Perempuan', 2, 'Zachary Hooper', 'Wynter Chambers', 'Preston Sandoval', 'Dolor doloremque rer', NULL),
(135, 90, 'erina3@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Erina 3', 'Perempuan', 3, 'Adrienne Dominguez', 'Judah Craig', 'Ivory Petersen', 'Sit id dolorem repe', NULL),
(136, 50, 'erina4@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Erina 4', 'Laki-laki', 4, 'Clinton Gilliam', 'Naida Acevedo', 'Madeline Browning', 'Non quo non reiciend', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `paket_kelas`
--

CREATE TABLE `paket_kelas` (
  `id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `paket_kelas`
--

INSERT INTO `paket_kelas` (`id`, `nama`) VALUES
(1, 'Ase (Berhitung)'),
(2, 'K-2013 PaketAhe (Membaca)'),
(3, 'Lets go (Bahasa Inggris)'),
(4, 'Mapel (Mata Pelajaran)');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` int(11) NOT NULL,
  `bulan_bayar` varchar(128) NOT NULL,
  `besaran` bigint(20) NOT NULL,
  `tahun` int(11) NOT NULL,
  `paket_kelas_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pembayaran`
--

INSERT INTO `pembayaran` (`id`, `bulan_bayar`, `besaran`, `tahun`, `paket_kelas_id`) VALUES
(27, 'Juli', 150000, 2021, 1),
(28, 'Juli', 150000, 2021, 2),
(29, 'Juli', 150000, 2021, 3),
(30, 'Juli', 150000, 2021, 4),
(31, 'Agustus', 150000, 2021, 1),
(32, 'Agustus', 150000, 2021, 2),
(33, 'Agustus', 150000, 2021, 3),
(34, 'Agustus', 150000, 2021, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tagihan`
--

CREATE TABLE `tagihan` (
  `id_tagihan` int(11) NOT NULL,
  `id_pembayaran` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `sudah_dibayar` int(11) DEFAULT NULL,
  `bukti_transfer` varchar(225) DEFAULT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tagihan`
--

INSERT INTO `tagihan` (`id_tagihan`, `id_pembayaran`, `id_siswa`, `sudah_dibayar`, `bukti_transfer`, `status`) VALUES
(163, 27, 128, 0, NULL, ''),
(164, 28, 129, 0, NULL, ''),
(165, 29, 135, 0, NULL, ''),
(166, 30, 136, 150000, '6e5a27d5b78e4a6e09299f9efb723825.png', 'lunas'),
(167, 31, 128, 0, NULL, ''),
(168, 32, 129, 0, NULL, ''),
(169, 33, 135, 0, NULL, ''),
(170, 34, 136, 0, NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `tagihan_id` int(11) NOT NULL,
  `jmlh_bayar` bigint(20) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `nama_petugas` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id`, `id_siswa`, `tagihan_id`, `jmlh_bayar`, `tgl_bayar`, `nama_petugas`) VALUES
(54, 136, 166, 150000, '2021-06-30', 'Admin Erina');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(7, 'Admin Erina', 'erina@gmail.com', 'profile_user.jpg', '21232f297a57a5a743894a0e4a801fc3', 1, 1, 1571583076),
(8, 'Baili Suhada', 'viola@gmail.com', 'user-profile-icon-7.jpg', '$2y$10$U6Hkgc36mCMhrqnROBRBKOnQHI2G1JrsC92gYm23Gy9p1PcDLbNjO', 2, 1, 1576980466),
(9, 'coba', 'coba@gmail.com', 'user-profile-icon-7.jpg', '$2y$10$U6Hkgc36mCMhrqnROBRBKOnQHI2G1JrsC92gYm23Gy9p1PcDLbNjO', 1, 1, 1571583076);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Walikelas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `walikelas`
--

CREATE TABLE `walikelas` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `kelas_id` int(11) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_siswa`
--
ALTER TABLE `data_siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indeks untuk tabel `paket_kelas`
--
ALTER TABLE `paket_kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tagihan`
--
ALTER TABLE `tagihan`
  ADD PRIMARY KEY (`id_tagihan`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `walikelas`
--
ALTER TABLE `walikelas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_siswa`
--
ALTER TABLE `data_siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT untuk tabel `paket_kelas`
--
ALTER TABLE `paket_kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT untuk tabel `tagihan`
--
ALTER TABLE `tagihan`
  MODIFY `id_tagihan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `walikelas`
--
ALTER TABLE `walikelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
