<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tagihan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //is_logged_in();
        $this->load->model('Masters_model');
        $this->load->model('Mtransaksi');
    }

    public function pilih_pembayaran()
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            $data['title']  = 'Master';
            $data['user']   = $this->db->get_where('user', ['email' => $this->session->userdata('admin_email')])->row_array();

            $paket_kelas = $this->input->post('paket_kelas');
            $data['paket_kelas'] = $this->input->post('paket_kelas');
            $data['tampil_pembayaran'] = $this->db->query("SELECT 
                                                                pembayaran.id AS id_pembayaran, bulan_bayar
                                                                FROM pembayaran
                                                                JOIN paket_kelas ON 
                                                                paket_kelas.id=pembayaran.paket_kelas_id
                                                                WHERE pembayaran.paket_kelas_id = '$paket_kelas'
                                                        ")->result_array();

            $data['tampil_siswa_sesuai_paket'] = $this->Masters_model->tampil_siswa_sesuai_paket($paket_kelas);

            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/master', $data);
            $this->load->view('admin/templates/footer');
        }
    }

    // Insert data pembeayaran dan tagihan
    // data tagihan otoamtis ke tambah sesuai paket siswa ketika ada data pembayaran masuk
    public function insert_pembayaran()
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            $bulan = $this->input->post('bulan', true);
            $tahun = $this->input->post('tahun', true);
            $paket = $this->input->post('paket_kelas', true);
            $cek_data = $this->db->query("SELECT * FROM pembayaran WHERE bulan_bayar='$bulan' AND tahun='$tahun' AND paket_kelas_id='$paket' LIMIT 1");
            if($cek_data->num_rows() > 0){
                //$q = $cek_data->row_array();
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    <b>Gagal!</b> Data Pembayaran sudah ada.</div>
                ');
                redirect('admin/master');
            }else{
                $data = [
                    'bulan_bayar'       => $this->input->post('bulan', true),
                    'besaran'           => $this->input->post('besaran', true),
                    'tahun'             => $this->input->post('tahun', true),
                    'paket_kelas_id'    => $this->input->post('paket_kelas', true),
                ];
        
                $this->db->insert('pembayaran', $data);
                $id_pembayaran = $this->db->insert_id();

                $id_siswa = $this->input->post('id_siswa');
                $result = array();
                foreach($id_siswa AS $key){
                    $result[] = array(
                    'id_pembayaran'  	=> $id_pembayaran,
                    'id_siswa'  	    => $key,
                    'sudah_dibayar'  	=> 0    
                    );
                }      
                $this->db->insert_batch('tagihan', $result);

                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    <b>Sukses</b> Berhasil Input Data Pembayaran <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
                redirect('admin/master');
            }
           
        }
    }
}