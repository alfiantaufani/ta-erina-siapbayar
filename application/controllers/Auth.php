<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        if ($this->session->userdata('email') != FALSE) {
            redirect('user');
        }

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'SIAP-bayar Login';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/login');
            $this->load->view('templates/auth_footer');
        } else {
            // jika validasinya success
            $this->_login();
        }
    }

    private function _login()
    {
        $email      = htmlspecialchars($this->input->post('email',TRUE),ENT_QUOTES);//ambil form
        $password   = htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);//ambil form

        //selek di tbl user
        $admin = $this->db->query("SELECT * FROM user WHERE email='$email' AND password=MD5('$password') LIMIT 1");

        //$userWalikelas = $this->db->get_where('walikelas', ['email' => $email])->row_array();
        
        if($admin->num_rows() > 0){
            $q = $admin->row_array();
            
            $this->session->set_userdata('admin_masuk',TRUE);
            $this->session->set_userdata('akses',$q['role_id']);
            $this->session->set_userdata('admin_email',$q['email']);
            $this->session->set_userdata('admin_id',$q['id']);
            $this->session->set_userdata('admin_nama',$q['name']);
            //redirect('admin');
            if ($q['role_id'] == 1) { //jika role id 1 masuk ke admin
                redirect('admin');
            } else {                     //jika role id bukan 1 masuk ke wali kelas
                redirect('walikelas');
            }
            
        }else{
            $siswa = $this->db->query("SELECT * FROM data_siswa WHERE email='$email' AND password = MD5('$password') LIMIT 1");

            if ($siswa->num_rows() > 0) {
                $q = $siswa->row_array();

                $this->session->set_userdata('siswa_masuk',TRUE);
                $this->session->set_userdata('akses','2');
                $this->session->set_userdata('siswa_email',$q['email']);
                $this->session->set_userdata('siswa_id',$q['id_siswa']);
                redirect('user');
            }else{
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email atau Password anda salah!</div>');
                redirect('auth');
            }
            

        }

        //jika User ada
        // if ($user['is_active'] == 1) {
        //     // cek password
        //     if (password_verify($password, $user['password'])) {
        //         $data = [
        //             'email' => $user['email'],
        //             'role_id' => $user['role_id']
        //         ];

        //         $this->session->set_userdata($data);
        //         if ($user['role_id'] == 1) {
        //             redirect('admin');
        //         } else {
        //             redirect('user');
        //         }
        //     } else {
        //         $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
        //         Password Salah!</div>
        //         ');
        //         redirect('auth');
        //     }
        // } else {
        //     $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
        //     email belum di aktifkan!</div>
        //     ');
        //     redirect('auth');
        // }
    }


    public function registration()
    {
            $data['title'] = 'SIAP-bayar Registration';
            $data['kelas'] = $this->db->query('SELECT * FROM paket_kelas')->result_array();
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/registration');
            $this->load->view('templates/auth_footer');        
    }

    public function register()
    {
        $email = $this->input->post('email');
        $nik   = $this->input->post('nik_siswa');
        
        $cek_email = $this->db->query("SELECT * FROM data_siswa WHERE email='$email' AND nik='$nik' LIMIT 1");
        if($cek_email->num_rows() > 0){
            $q = $cek_email->row_array();
            $this->session->set_flashdata('message_email', '<div class="alert alert-danger" role="alert">
                Maaf! Email atau NIK yang anda masukkan sudah terdaftar</div>
            ');
            redirect('auth/registration');
        }else{
            
            $password1 = $this->input->post('password1');
            $password2 = $this->input->post('password2');

            if($password1 != $password2){
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    Maaf! Password anda tidak sama</div>
                ');
                redirect('auth/registration');
            }else{
                $data = [
                    'nik'           => $this->input->post('nik_siswa'),
                    'email'         => htmlspecialchars($email),
                    'password'      => md5($this->input->post('password1')),
                    'nama_siswa'    => $this->input->post('nama_siswa'),
                    'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                    'paket_kelas_id'   => $this->input->post('paket_kelas'),
                    'nama_ayah'     => $this->input->post('nama_ayah'),
                    'nama_ibu'      => $this->input->post('nama_ibu'),
                    'no_hp_ortu'    => $this->input->post('no_hp_ortu'),
                    'alamat_ortu'   => $this->input->post('alamat_ortu')
                ];
                $this->db->insert('data_siswa', $data);
                $id_siswa = $this->db->insert_id();

                $id_paket_kelas = $this->input->post('paket_kelas');
                $cek_pembayaran = $this->db->query("SELECT id FROM pembayaran WHERE paket_kelas_id='$id_paket_kelas'")->result();

                foreach($cek_pembayaran AS $key){
                    $result = [
                        $key->id
                    ];
                    foreach ($result as $a) {
                        $ha = [
                            'id_pembayaran' => $a,
                            'id_siswa'      => $id_siswa,
                            'sudah_dibayar' => 0,
                        ];$this->db->insert('tagihan', $ha);
                    }
                    
                } 
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Selamat! anda berhasil terdaftar. Silahkan login</div>
                ');
                redirect('auth');
            }
        }
        


        

        // siapkan token
        // $token = base64_encode(random_bytes(32));
        // $user_token = [
        //     'email'        => $email,
        //     'token'        => $token,
        //     'date_created' => time()
        // ];

        
        // $this->db->insert('user_token', $user_token);

        // $this->_sendEmail($token, 'verify');

        // $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        // Selamat! akun berhasil di registrasi. silahkan periksa email untuk aktivasi akun</div>
        // ');
        // redirect('auth');
    }

    private function _sendEmail($token, $type)
    {
        $config = [
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_user' => 'siapbayaronline@gmail.com',
            'smtp_pass' => 'rahasiasangat',
            'smtp_port' => 465,
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'newline'   => "\r\n"
        ];

        $this->load->library('email', $config);
        $this->email->initialize($config);

        $this->email->from('siapbayaronline@gmail.com', 'SIAP Bayar');
        $this->email->to($this->input->post('email'));

        if ($type == 'verify') {
            $this->email->subject('Kode Aktivasi Akun SIAP Bayar');
            $this->email->message('silahkan klick tombol berikut untuk aktivasi akun SIAP Bayar : <a href="' . base_url() . 'auth/verify?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '" type="button" style="color: green;">Activate</a>');
        } else if ($type == 'forgot') {
            $this->email->subject('Reset Password Akun SIAP Bayar');
            $this->email->message('silahkan klick tombol berikut untuk reset password akun SIAP Bayar : <a href="' . base_url() . 'auth/resetpassword?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '" type="button" style="color: green;">Reset Password</a>');
        }


        if ($this->email->send()) {
            return true;
        } else {
            echo $this->email->print_debugger();
            die;
        }
    }

    public function verify()
    {
        $email = $this->input->get('email');
        $token = $this->input->get('token');

        $user = $this->db->get_where('user', ['email' => $email])->row_array();

        if ($user) {
            $user_token = $this->db->get_where('user_token', ['token' => $token])->row_array();

            if ($user_token) {
                if (time() - $user_token['date_created'] < (60 * 60 * 24)) {
                    $this->db->set('is_active', 1);
                    $this->db->where('email', $email);
                    $this->db->update('user');

                    $this->db->delete('user_token', ['email' => $email]);
                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">' . $email . '
                    berhasil diaktifkan, silahkan login!</div>
                    ');
                    redirect('auth');
                } else {

                    $this->db->delete('user', ['email' => $email]);
                    $this->db->delete('user_token', ['email' => $email]);

                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    Aktivasi gagal, token anda expired!</div>
                    ');
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
        Aktivasi gagal, token anda salah!</div>
        ');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
        Aktivasi gagal, email salah!</div>
        ');
            redirect('auth');
        }
    }


    public function logout()
    {
        $this->session->sess_destroy();;
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        anda sudah logged out!</div>
        ');
        redirect('auth');
    }


    public function blocked()
    {
        $this->load->view('auth/blocked');
    }

    public function forgotPassword()
    {
        $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'SIAP-bayar Forgot Password';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/forgotpassword');
            $this->load->view('templates/auth_footer');
        } else {
            $email = $this->input->post('email');
            $user = $this->db->get_where('user', ['email' => $email, 'is_active' => 1])->row_array();

            if ($user) {
                // siapkan token
                $token = base64_encode(random_bytes(32));
                $user_token = [
                    'email' => $email,
                    'token' => $token,
                    'date_created' => time()
                ];

                $this->db->insert('user_token', $user_token);
                $this->_sendEmail($token, 'forgot');
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Cek email anda untuk reset password</div>
                ');
                redirect('auth');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Email belum di registrasi atau belum diaktifkan</div>
                ');
                redirect('auth/forgotpassword');
            }
        }
    }

    public function resetPassword()
    {
        $email = $this->input->get('email');
        $token = $this->input->get('token');

        $user = $this->db->get_where('user', ['email' => $email])->row_array();

        if ($user) {
            // jika user ada.
            $user_token = $this->db->get_where('user_token', ['token' => $token])->row_array();
            if ($user_token) {
                // jika token ada
                // buat session untuk change password.
                $this->session->set_userdata('reset_email', $email);
                $this->changePassword();
            } else {
                // jika token tidak ada
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Reset password gagal !, token salah.</div>
            ');
                redirect('auth');
            }
        } else {
            // jika user tidak ada
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Reset password gagal !, email salah.</div>
            ');
            redirect('auth');
        }
    }

    public function changePassword()
    {
        if (!$this->session->userdata('reset_email')) {
            redirect('auth');
        }

        $this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[6]|matches[password2]');
        $this->form_validation->set_rules('password2', 'Repeat Password', 'trim|required|min_length[6]|matches[password1]');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'SIAP-bayar Change Password';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('auth/changepassword');
            $this->load->view('templates/auth_footer');
        } else {
            $password = password_hash($this->input->post('password1'), PASSWORD_DEFAULT);
            $email = $this->session->userdata('reset_email');

            $this->db->set('password', $password);
            $this->db->where('email', $email);
            $this->db->update('user');

            // hapus sesi change password
            $this->session->unset_userdata('reset_email');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            password berhasil diganti !, silahkan login.</div>
            ');
            redirect('auth');
        }
    }
}
