<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //is_logged_in();
        $this->load->model('Muser');
        $this->load->model('Mtransaksi');
        $this->load->library('user_agent');
        $this->load->helper(array('form', 'url', 'file'));
        $this->load->library('upload','session', 'database');
    }

    public function index()
    {
        if($this->session->userdata('siswa_masuk') != TRUE){
			redirect('auth');
		}else{
			$data['title'] = 'Beranda';
            $data['user'] = $this->db->get_where('data_siswa', ['email' => $this->session->userdata('siswa_email')])->row_array();

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('user/index', $data);
            $this->load->view('templates/footer', $data);
		}
    }

    public function profil_siswa()
    {
        if($this->session->userdata('siswa_masuk') != TRUE){
			redirect('auth');
		}else{
			$data['title'] = 'Profil Siswa';
            $data['user'] = $this->Muser->data_siswa();
            $data['kelas'] = $this->db->query('SELECT * FROM paket_kelas')->result_array();
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('user/profil', $data);
            $this->load->view('templates/footer', $data);
		}
    }

    public function edit_siswa()
    {
        if($this->session->userdata('siswa_masuk') != TRUE){
			redirect('auth');
		}else{
            $data = [
                'nik'           => $this->input->post('nik'),
                'nama_siswa'    => $this->input->post('nama_siswa'),
                'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                'paket_kelas_id' => $this->input->post('paket_kelas'),
                'nama_ayah'     => $this->input->post('nama_ayah'),
                'nama_ibu'      => $this->input->post('nama_ibu'),
                'no_hp_ortu'    => $this->input->post('no_hp_ortu'),
                'alamat_ortu'   => $this->input->post('alamat_ortu')
            ];
            $this->db->where('email', $this->input->post('email'));
            $this->db->update("data_siswa", $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                <b>Sukses!</b> Berhasil edit profil<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
            ');
            redirect('user/profil_siswa');
        }
    }

    public function pembayaran()
    {
        if($this->session->userdata('siswa_masuk') != TRUE){
			redirect('auth');
		}else{
			$data['title'] = 'Pembayaran';
            $data['user'] = $this->db->get_where('data_siswa', ['email' => $this->session->userdata('siswa_email')])->row_array();
            $id_siswa = $this->session->userdata('siswa_id');

            //$data['transfer'] = $this->Muser->data_transfer();
            $data['tagihan'] = $this->Muser->data_tagihan();
            $data['transaksi'] = $this->Muser->data_transaksi();

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('user/pembayaran', $data);
            $this->load->view('templates/footer', $data);
		}
    }

    public function input_pembayaran_by_siswa()
    {
        if($this->session->userdata('siswa_masuk') != TRUE){
			redirect('auth');
		}else{

            $config['upload_path']          = './assets/img/img_bukti';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            // $config['max_size']             = 100;
            // $config['max_width']            = 1024;
            // $config['max_height']           = 768;
            $config['encrypt_name']			= TRUE;
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('berkas')) 
            {
                // $error = array('error' => $this->upload->display_errors());
                // var_dump($error);
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    <b>Error!</b> Gagal melakukan transaksi <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
                redirect('user/pembayaran');
            }else{
                $bukti_bayar = $this->upload->data("file_name");
                $id_siswa = $this->input->post('id_siswa');
                $id_tagihan = $this->input->post('id_tagihan');
                $name = $this->upload->data("file_name");
                //var_dump($id_tagihan);
                $data = [
                    "bukti_transfer" 		=> $name,
                    "status" 		        => "pending"
                ];
        
                $this->db->where('id_tagihan', $id_tagihan);
                $this->db->update('tagihan', $data);
                
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    <b>Sukses!</b> Berhasil melakukan transaksi <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
                redirect('user/pembayaran');
            }

            
        }
    }

    public function print_transaksi($id_transaksi)
    {
        require_once __DIR__ . '/vendor/autoload.php';
        if($this->session->userdata('siswa_masuk') != TRUE){
			redirect('auth');
		}else{
            //$data['data_siswa'] = $this->MyModel->data_siswa();
			$data['tampil_transaksi'] = $this->Mtransaksi->tampil_transaksi_print($id_transaksi);

			$data['title'] = "Print Bukti";

			$mpdf = new \Mpdf\Mpdf();
			$html = $this->load->view('user/print_bukti',$data, TRUE);
			$mpdf->AddPage("P","","","","","","10","10","10","10","","","","","","","","","","","","A4");

			$mpdf->WriteHTML($html);
			$mpdf->Output();
        }
    }

    public function edit()
    {
        $data['title'] = 'Edit Profile';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->form_validation->set_rules('name', 'Full Name', 'required|trim');

        if ($this->form_validation->run() == false) {

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('user/edit', $data);
            $this->load->view('templates/footer');
        } else {
            $name = $this->input->post('name');
            $email = $this->input->post('email');

            // cek jika ada gambar yang akan di upload
            $upload_image = $_FILES['image']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size']      = '2048'; //dalam kilo bite(kb)
                $config['upload_path']   = './assets/img/profile/';

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {

                    $old_image = $data['user']['image'];

                    if ($old_image != 'default.jpg') {
                        unlink(FCPATH . 'assets/img/profile/' . $old_image);
                    }

                    $new_image = $this->upload->data('file_name');
                    $this->db->set('image', $new_image);
                } else {

                    echo $this->upload->display_errors();
                }
            }

            $this->db->set('name', $name);
            $this->db->where('email', $email);
            $this->db->update('user');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Selamat! profile berhasil diubah!</div>
            ');
            redirect('user');
        }
    }

    public function changePassword()
    {
        $data['title'] = 'Change Password';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->form_validation->set_rules('current_password', 'Current Password', 'required|trim');
        $this->form_validation->set_rules('new_password1', 'New Password', 'required|trim|min_length[6]|matches[new_password2]');
        $this->form_validation->set_rules('new_password2', 'Confirm New Password', 'required|trim|min_length[6]|matches[new_password1]');

        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('user/changepassword', $data);
            $this->load->view('templates/footer', $data);
        } else {
            $current_password = $this->input->post('current_password');
            $new_password     = $this->input->post('new_password1');
            if (!password_verify($current_password, $data['user']['password'])) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Password lama salah!</div>
            ');
                redirect('user/changepassword');
            } else {
                if ($current_password == $new_password) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Password baru tidak boleh sama dengan password lama!</div>
            ');
                    redirect('user/changepassword');
                } else {
                    $password_hash = password_hash($new_password, PASSWORD_DEFAULT);

                    $this->db->set('password', $password_hash);
                    $this->db->where('email', $this->session->userdata('email'));
                    $this->db->update('user');

                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Password berhasil di ganti!</div>
            ');
                    redirect('user/changepassword');
                }
            }
        }
    }
}
