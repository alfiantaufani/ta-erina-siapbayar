<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RiwayatUser extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //is_logged_in();
        $this->load->model('Muser');
        $this->load->model('Mtransaksi');
        $this->load->library('user_agent');
        $this->load->helper(array('form', 'url', 'file'));
        $this->load->library('upload','session', 'database');
    }

    public function riwayat_pembayaran()
    {
        if($this->session->userdata('siswa_masuk') != TRUE){
			redirect('auth');
		}else{
			$data['title'] = 'Riwayat Pembayaran';
            $data['user'] = $this->db->get_where('data_siswa', ['email' => $this->session->userdata('siswa_email')])->row_array();
            $id_siswa = $this->session->userdata('siswa_id');

            $data['transfer_pending'] = $this->Muser->transfer_pending();
            $data['transfer_sukses'] = $this->Muser->transfer_sukses();
            $data['transaksi'] = $this->Muser->data_transaksi();

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('user/riwayat_pembayaran', $data);
            $this->load->view('templates/footer', $data);
		}
    }
}