<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //is_logged_in();
        $this->load->model('Masters_model');
        $this->load->model('Mtransaksi');
    }

    public function index()
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            $data['title'] = 'Dashboard';
            $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('admin_email')])->row_array();

            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/index', $data);
            $this->load->view('admin/templates/footer', $data);
        }
        
    }

    // CRUD DATA MASTER
    public function master()
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            $data['title']  = 'Master';
            $data['user']   = $this->db->get_where('user', ['email' => $this->session->userdata('admin_email')])->row_array();
            //$data['pembayaran']  = $this->Masters_model->data_pembayaran();
            $data['pembayaran']  = 
            $this->db->query('SELECT pembayaran.id AS id_pembayaran, 
                                pembayaran.bulan_bayar AS bulan_bayar, 
                                pembayaran.besaran AS besaran, 
                                pembayaran.tahun AS tahun, 
                                pembayaran.paket_kelas_id AS paket_kelas_id,
                                paket_kelas.nama AS nama, 
                                paket_kelas.id AS id_kelas 
                                FROM pembayaran
                                INNER JOIN paket_kelas 
                                ON paket_kelas.id = pembayaran.paket_kelas_id
                            ')->result_array();
            $data['tagihan']  = $this->Masters_model->data_tagihan();
            $data['paket_kelas']  = $this->Masters_model->data_paket_kelas();

            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/master', $data);
            $this->load->view('admin/templates/footer');
        }
    }

    public function input_paket()
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            $data = [
                'nama' => $this->input->post('nama')
            ];
            $this->db->insert("paket_kelas", $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                <b>Sukses!</b> Berhasil tambah data paket kelas <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
            ');
            redirect('admin/master');
        }
    }

    public function update_pembayaran()
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            $bulan = $this->input->post('bulan', true);
            $tahun = $this->input->post('tahun', true);
            $paket = $this->input->post('paket_kelas', true);
            $cek_data = $this->db->query("SELECT * FROM pembayaran WHERE bulan_bayar='$bulan' AND tahun='$tahun' AND paket_kelas_id='$paket' LIMIT 1");
            if($cek_data->num_rows() >= 2){
                //$q = $cek_data->row_array();
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    <b>Gagal!</b> Data Pembayaran sudah ada.</div>
                ');
                redirect('admin/master');
            }else{
                $id_pembayaran = $this->input->post('id_pembayaran');
                $data = [
                    'bulan_bayar'       => $this->input->post('bulan'),
                    'besaran'           => $this->input->post('besaran'),
                    'tahun'             => $this->input->post('tahun'),
                    'paket_kelas_id'    => $this->input->post('paket_kelas')
                ];
                $this->db->where('id', $id_pembayaran);
                $this->db->update("pembayaran", $data);
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    <b>Sukses!</b> Berhasil Edit Data Pembayaran <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
                redirect('admin/master');
            }
            
        }
    }

    public function delete_pembayaran($id_pembayaran)
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            $this->db->delete('pembayaran', array('id' => $id_pembayaran));
            $this->db->delete('tagihan', array('id_pembayaran' => $id_pembayaran));

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                <b>Sukses!</b> Berhasil Hapus Data Pembayaran <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
            ');
            redirect('admin/master');
        }
    }

    public function update_paket()
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            $id_paket = $this->input->post('id_paket');
            $data = [
                'nama'       => $this->input->post('nama')
            ];
            $this->db->where('id', $id_paket);
            $this->db->update("paket_kelas", $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                <b>Sukses!</b> Berhasil Edit Data Paket Kelas <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
            ');
            redirect('admin/master');
        }
    }

    //END CRUD DATA MASTER 

    public function data_master_siswa()
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            $data['title']  = 'Data Master Siswa';
            $data['user']   = $this->db->get_where('user', ['email' => $this->session->userdata('admin_email')])->row_array();
            $data['data_siswa']  = $this->Masters_model->data_siswa();
            $data['paket_kelas']  = $this->Masters_model->data_paket_kelas();

            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/data_master_siswa', $data);
            $this->load->view('admin/templates/footer');
        }
    }

    public function edit_siswa($id_siswa)
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            $data['title']  = 'Edit Data Siswa';
            $data['user']   = $this->db->get_where('user', ['email' => $this->session->userdata('admin_email')])->row_array();
            $data['data_siswa'] = $this->Masters_model->data_siswa_edit($id_siswa);
            $data['paket_kelas']  = $this->Masters_model->data_paket_kelas();

            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/data_master_siswa', $data);
            $this->load->view('admin/templates/footer');
        }
    }

    public function update_siswa()
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            $email = $this->input->post('email');
            $cek_email = $this->db->query("SELECT * FROM data_siswa WHERE email='$email' LIMIT 1");
            if($cek_email->num_rows() > 1){
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    Maaf! Email yang anda masukkan sudah terdaftar</div>
                ');
                redirect('admin/edit_siswa/'. $this->input->post('id').'');
            }else{
                $data = [
                    'nik'           => $this->input->post('nik'),
                    'email'           => $this->input->post('email'),
                    'nama_siswa'    => $this->input->post('nama_siswa'),
                    'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                    'paket_kelas_id' => $this->input->post('paket_kelas'),
                    'nama_ayah'     => $this->input->post('nama_ayah'),
                    'nama_ibu'      => $this->input->post('nama_ibu'),
                    'no_hp_ortu'    => $this->input->post('no_hp_ortu'),
                    'alamat_ortu'   => $this->input->post('alamat_ortu')
                ];
                $this->db->where('id_siswa', $this->input->post('id_siswa'));
                $this->db->update("data_siswa", $data);
                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    <b>Sukses</b> Update Data <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
                redirect('admin/data_master_siswa');
            }
            
        }
    }

    public function hapus_siswa($id)
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{

            $this->db->delete('data_siswa', array('id_siswa' => $id)); 
            $this->db->delete('tagihan', array('id_siswa' => $id));
            
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    <b>Sukses</b> Hapus Data <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
            redirect('admin/data_master_siswa');
        }
    }

    public function transaksi()
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            $data['title']  = 'Transaksi';
            $data['user']   = $this->db->get_where('user', ['email' => $this->session->userdata('admin_email')])->row_array();
            
            $data['data_siswa']   = $this->Masters_model->data_siswa();
            $data['tampil_tagihan'] = $this->Mtransaksi->tampil_tagihan_siswa();
            

            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/transaksi', $data);
            $this->load->view('admin/templates/footer');
        }
    }

    public function cari_tagihan_siswa()
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            $data['title']  = 'Transaksi';
            $data['user']   = $this->db->get_where('user', ['email' => $this->session->userdata('admin_email')])->row_array();

            $id_siswa = $this->uri->segment(3);
            
            $data['tampil_tagihan'] = $this->Mtransaksi->tampil_tagihan_siswa_id($id_siswa);
            $data['tampil_tagihan_pending'] = $this->Mtransaksi->tampil_tagihan_pending($id_siswa);
            $data['tampil_tagihan_lunas'] = $this->Mtransaksi->tampil_tagihan_lunas($id_siswa);
            $data['data_siswa'] = $this->Mtransaksi->data_siswa($id_siswa);
            $data['tampil_transaksi'] = $this->Mtransaksi->tampil_transaksi($id_siswa);

            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/transaksi', $data);
            $this->load->view('admin/templates/footer');
        }
    }

    public function input_pembayaran()
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            $sudah_dibayar = $this->input->post('sudah_dibayar');
            $nominal = $this->input->post('jumlah_bayar');
            $id_siswa = $this->input->post('id_siswa');
            $id_tagihan = $this->input->post('id_tagihan');

            $cek = $this->db->query("SELECT * FROM tagihan WHERE id_tagihan='$id_tagihan'");
            $cek_sudah_dibayar = $cek->row();
            $sdh_dibayar = $cek_sudah_dibayar->sudah_dibayar;
            //var_dump($nominal);
            
            if ($sdh_dibayar >= $nominal) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    <b>Gagal!</b> Nominal Bayar tidak boleh lebih dari tagihan <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
                ');
                $referred_from = $this->session->userdata('referred_from'); 
			    redirect($referred_from, 'refresh');
            }
            
            $hasil = $sudah_dibayar + $nominal;
            $data = [
                "sudah_dibayar" 		=> $hasil
            ];
            $this->db->where('id_tagihan', $id_tagihan);
            $this->db->update('tagihan', $data);

            
            $insert_transaksi = [
                'id_siswa' => $id_siswa,
                'tagihan_id' => $id_tagihan,
                'jmlh_bayar' => $nominal,
                'tgl_bayar' => date('Y-m-d'),
                'nama_petugas' =>  $this->session->userdata('admin_nama'),
            ];
            $this->db->insert("transaksi", $insert_transaksi);

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                <b>Sukses!</b> Berhasil melakukan pembayaran <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
            ');
            $referred_from = $this->session->userdata('referred_from'); 
            redirect($referred_from, 'refresh');
            
        }
    }

    public function konfirmasi()
    {
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            $id_tagihan =  $this->uri->segment(3);
            
            $data = [
                'status' => 'lunas'
            ];

            $this->db->where('id_tagihan', $id_tagihan);
            $this->db->update('tagihan', $data);
            
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                <b>Sukses!</b> Berhasil Mengkonfirmasi transaksi <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div>
            ');
            $referred_from = $this->session->userdata('referred_from'); 
            redirect($referred_from, 'refresh');
        }
    }

    public function print_transaksi($id_transaksi)
    {
        require_once __DIR__ . '/vendor/autoload.php';
        if($this->session->userdata('admin_masuk') != TRUE){
			redirect('auth');
		}else{
            //$data['data_siswa'] = $this->MyModel->data_siswa();
			$data['tampil_transaksi'] = $this->Mtransaksi->tampil_transaksi_print($id_transaksi);

			$data['title'] = "Print Bukti";

			$mpdf = new \Mpdf\Mpdf();
			$html = $this->load->view('admin/print_bukti',$data, TRUE);
			$mpdf->AddPage("P","","","","","","10","10","10","10","","","","","","","","","","","","A4");

			$mpdf->WriteHTML($html);
			$mpdf->Output();
        }
    }
}
