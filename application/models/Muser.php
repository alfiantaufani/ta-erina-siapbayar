<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Muser extends CI_Model
{
    public function data_siswa()
    {
        $data_login = $this->session->userdata('siswa_email');
        $this->db->select('*');
        $this->db->from('data_siswa');
        $this->db->join('paket_kelas', 'data_siswa.paket_kelas_id = paket_kelas.id');
        $this->db->where('data_siswa.email',$data_login);
        $result= $this->db->get()->row_array();
		return $result;  
    }

    public function transfer_pending()
    {
        $pending = "pending";
        $id_siswa = $this->session->userdata('siswa_id');
        $this->db->select('*');
		$this->db->from('tagihan');
        $this->db->join('pembayaran','tagihan.id_pembayaran=pembayaran.id');
        $this->db->join('data_siswa','tagihan.id_siswa=data_siswa.id_siswa');
        $this->db->join('paket_kelas','data_siswa.paket_kelas_id=paket_kelas.id');
        $this->db->where('tagihan.id_siswa', $id_siswa);
        $this->db->where('tagihan.status', $pending);
        //$this->db->group_by('tagihan.id_tagihan');
        $result= $this->db->get()->result_array();
		return $result;
    }

    public function transfer_sukses()
    {
        $lunas = "lunas";
        $id_siswa = $this->session->userdata('siswa_id');
        $this->db->select('*');
		$this->db->from('tagihan');
        $this->db->join('pembayaran','tagihan.id_pembayaran=pembayaran.id');
        $this->db->join('data_siswa','tagihan.id_siswa=data_siswa.id_siswa');
        $this->db->join('paket_kelas','data_siswa.paket_kelas_id=paket_kelas.id');
        $this->db->where('tagihan.id_siswa', $id_siswa);
        $this->db->where('tagihan.status', $lunas);
        //$this->db->group_by('tagihan.id_tagihan');
        $result= $this->db->get()->result_array();
		return $result;
    }

    public function data_tagihan()
    {
        $id_siswa = $this->session->userdata('siswa_id');
        $this->db->select('*');
        $this->db->from('tagihan');
        $this->db->join('pembayaran', 'tagihan.id_pembayaran = pembayaran.id');
        $this->db->join('data_siswa', 'tagihan.id_siswa = data_siswa.id_siswa');
        $this->db->join('paket_kelas', 'data_siswa.paket_kelas_id = paket_kelas.id');
        $this->db->where('data_siswa.id_siswa',$id_siswa);
        $result= $this->db->get()->result_array();
		return $result;
    }

    public function data_transaksi()
    {
        $id_siswa = $this->session->userdata('siswa_id');
        $this->db->select('*');
        $this->db->from('transaksi');
        $this->db->join('data_siswa', 'transaksi.id_siswa = data_siswa.id_siswa');
        $this->db->join('tagihan', 'transaksi.tagihan_id = tagihan.id_tagihan');
        $this->db->join('pembayaran', 'tagihan.id_pembayaran = pembayaran.id');
        $this->db->join('paket_kelas', 'data_siswa.paket_kelas_id = paket_kelas.id');

        $this->db->where('transaksi.id_siswa',$id_siswa);
        $result= $this->db->get()->result_array();
		return $result;  
    }
}