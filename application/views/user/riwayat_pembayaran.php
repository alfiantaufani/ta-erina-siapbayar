<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-primary"><?= $title; ?></h1>

    <div class="row">
        <div class="col-lg-12">
            <?= $this->session->flashdata('message'); ?>
        </div>
    </div>

    <!-- row untuk jadi satu baris card -->
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-primary">Daftar Bukti Transfer <i>(Diproses)</i></h5>
                </div>
                <div class="card-body">
                    
                        <div class="table-responsive">
                            <table class="table table-bordered" id="tableIuran">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Tagihan Bulan</th>
                                        <th scope="col">Tahun</th>
                                        <th scope="col">Harus Dibayar</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Bukti Bayar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; foreach ($transfer_pending as $t) : ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $t['bulan_bayar']; ?></td>
                                            <td><?= $t['tahun']; ?></td>
                                            <td>
                                                <?php 
                                                    $angka = $t['besaran'];
                                                    $rupiah = "Rp. " . number_format($angka, 2, ',', '.');
                                                    echo $rupiah;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($t['status'] == 'lunas') {
                                                    echo '<h5><span class="badge badge-info">Sukses</span></h5>';
                                                }else{
                                                    echo '<h5><span class="badge badge-warning">Pending</span></h5>
                                                    ';
                                                }
                                            ?>
                                            </td>
                                            <td>
                                                <a href="<?= base_url('assets/img/img_bukti')?>/<?=$t['bukti_transfer'];?>" target="_blank" class="btn btn-info"> Lihat Bukti Transfer
                                                    <!-- <img src="<?= base_url('assets/img/img_bukti')?>/<?=$t['bukti_transfer'];?>" alt="" width="100"> -->
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    
                </div>
            </div> 
        </div>  

        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-primary">Daftar Bukti Transfer <i>(Sukses)</i></h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tableKelas">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Tagihan Bulan</th>
                                    <th scope="col">Tahun</th>
                                    <th scope="col">Harus Dibayar</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Bukti Bayar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($transfer_sukses as $sukses) : ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $sukses['bulan_bayar']; ?></td>
                                        <td><?= $sukses['tahun']; ?></td>
                                        <td>
                                            <?php 
                                                $angka = $sukses['besaran'];
                                                $rupiah = "Rp. " . number_format($angka, 2, ',', '.');
                                                echo $rupiah;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if ($sukses['status'] == 'lunas') {
                                                echo '<h5><span class="badge badge-info">Sukses</span></h5>';
                                            }else{
                                                echo '<h5><span class="badge badge-warning">Pending</span></h5>
                                                ';
                                            }
                                        ?>
                                        </td>
                                        <td>
                                            <a href="<?= base_url('assets/img/img_bukti')?>/<?=$sukses['bukti_transfer'];?>" target="_blank" class="btn btn-info"> Lihat Bukti Transfer
                                                <!-- <img src="<?= base_url('assets/img/img_bukti')?>/<?=$sukses['bukti_transfer'];?>" alt="" width="100"> -->
                                            </a>
                                        </td>
                                        
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>                

        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-primary">Riwayat Transaksi Pembayaran Siswa</h5>
                </div>
                <div class="card-body">
                <div class="table-responsive">
                        <table class="table table-bordered" id="tableSemester">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Kode Pembayaran</th>
                                    <th scope="col">Jenis Pembayaran</th>
                                    <th scope="col">Tanggal</th>
                                    <th scope="col">Nominal Bayar</th>
                                    <th scope="col">Penerima</th>
                                    <th scope="col">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($transaksi as $t) : ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $t['id']; ?><?= preg_replace('/-/', '', $t['tgl_bayar']) ; ?></td>
                                        <td><?= $t['bulan_bayar']; ?></td>
                                        <td>
                                            <?= date('d F Y', strtotime($t['tgl_bayar'])); ?>
                                        </td>
                                        <td>
                                            <?php 
                                                $angka = $t['jmlh_bayar'];
                                                $rupiah = "Rp " . number_format($angka, 2, ',', '.');
                                                echo $rupiah;
                                            ?>
                                        </td>
                                        <td><?= $t['nama_petugas']; ?></td>
                                        
                                        <td class="text-center">
                                            <a href="#detailModal<?= $t['id']; ?>" data-toggle="modal" class="btn btn-info">
                                                <i class="fas fa-fw fa-eye fa-sm"></i> Lihat Bukti
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>

                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <!-- /.end raw card -->

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<?php foreach ($transaksi as $t) : ?>
    <div class="modal fade" id="detailModal<?= $t['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Trasnsaksi Siswa</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="invoice-title">
                            <h4 class="lead text-gray-800 d-none d-lg-block text-center">
                                <img src="<?= base_url('assets/'); ?>logo/siap-bayar-top.png" alt="logo-image" class="img-circle">Bimbingan Belajar Ahe Morosunggingan
                            </h4>
                            <h2><center><b>Bukti Pembayaran</b></center></h2>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <address>
                                <strong>Nama Siswa :</strong><br>
                                <?= $t['nama_siswa']; ?>
                                </address>
                                <address>
                                    <strong>Paket Kelas :</strong><br>
                                    <?= $t['nama']; ?>
                                </address>
                            </div>
                            <div class="col-md-6 text-right">
                                <address>
                                <strong>Kode Pembayaran :</strong><br>
                                <?= $t['id']; ?><?= preg_replace('/-/', '', $t['tgl_bayar']) ; ?>
                                </address>

                                <address>
                                    <strong>Tanggal Pembayaran:</strong><br>
                                    <?= date('d F Y', strtotime($t['tgl_bayar'])); ?>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title"><strong>Untuk Pembayaran :</strong></h5>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-condensed">
                                        <thead>
                                            <tr>
                                                <td><strong>Jenis Pembayaran Bulan</strong></td>
                                                <td class="text-center"><strong></strong></td>
                                                <td class="text-right"><strong>Jumlah Bayar</strong></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                            <tr>
                                                <td><?= $t['bulan_bayar']; ?></td>
                                                <td class="text-center"></td>
                                                <td class="text-right">
                                                    <?php 
                                                        $angka = $t['jmlh_bayar'];
                                                        $rupiah = "Rp " . number_format($angka, 2, ',', '.');
                                                        echo $rupiah;
                                                    ?>
                                                </td>
                                                
                                            </tr>
                                            
                                            <tr>
                                                <td class="no-line"></td>
                                                <td class="no-line text-right"><strong>Total</strong></td>
                                                <td class="no-line text-right">
                                                <?php 
                                                        $angka = $t['jmlh_bayar'];
                                                        $rupiah = "Rp " . number_format($angka, 2, ',', '.');
                                                        echo $rupiah;
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                            <address>
                                Jombang, <?= date('d F Y', strtotime($t['tgl_bayar'])); ?><br>
                                Diterima Oleh<br><br><br>
                                <?= $t['nama_petugas']; ?>
                            </address>
                        </div>
                </div>

                </div>

                <div class="modal-footer">
                    <a href="<?= base_url('user/print_transaksi/'.$t['id'].'')?>" target="_blank" class="btn btn-info">
                        <i class="fas fa-fw fa-print fa-sm"></i> Print
                    </a>
                    <button class="btn btn-outline-info" data-dismiss="modal" aria-label="Close">
                    Tutup
                    </button>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
