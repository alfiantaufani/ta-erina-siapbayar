<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-primary"><?= $title; ?></h1>

    <div class="row">
        <div class="col-lg-12">
            <?= $this->session->flashdata('message'); ?>
        </div>
    </div>

    <!-- row untuk jadi satu baris card -->
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h5 class="m-0 font-weight-bold text-primary">Tagihan Siswa</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tableKelas">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Paket Kelas</th>
                                    <th scope="col">Tagihan Bulan</th>
                                    <th scope="col">Tahun</th>
                                    <th scope="col">Harus Dibayar</th>
                                    <th scope="col">Sudah Dibayar</th>
                                    <th scope="col">Kekurangan</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; foreach ($tagihan as $t) : ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $t['nama']; ?></td>
                                        <td><?= $t['bulan_bayar']; ?></td>
                                        <td><?= $t['tahun']; ?></td>
                                        <td>
                                            <?php 
                                                $angka = $t['besaran'];
                                                $rupiah = "Rp. " . number_format($angka, 2, ',', '.');
                                                echo $rupiah;
                                            ?>
                                        </td>
                                        <td><b>
                                            <?php 
                                                $angka = $t['sudah_dibayar'];
                                                $rupiah = "Rp. " . number_format($angka, 2, ',', '.');
                                                echo $rupiah;
                                            ?>
                                            </b>
                                        </td>
                                        <td><b>
                                            <?php //kekurangan
                                                $besaran = $t['besaran'];
                                                $sudah_dibayar = $t['sudah_dibayar'];
                                                $hasil = $besaran - $sudah_dibayar;
                                                $rupiah = "Rp. " . number_format($hasil, 2, ',', '.');
                                                echo $rupiah;
                                            ?>
                                            </b>
                                        </td>
                                        <td>
                                            <?php
                                                $besaran = $t['besaran'];
                                                $sudah_dibayar = $t['sudah_dibayar'];
                                                if ($sudah_dibayar === $besaran) {
                                                    echo '<h5><span class="badge badge-info">Lunas</span></h5>';
                                                }else{
                                                    echo '<h5><span class="badge badge-danger">Belum Lunas</span></h5>';
                                                }
                                            ?>
                                            
                                        </td>
                                        <td>
                                            <?php
                                                $besaran = $t['besaran'];
                                                $sudah_dibayar = $t['sudah_dibayar'];
                                                if ($sudah_dibayar == $besaran) {
                                                    echo '<h5><span class="badge badge-info">Lunas</span></h5>';
                                                }else{
                                                    echo '
                                                    <button type="button" data-toggle="modal" data-target="#modal'.$t['id_tagihan'].'" class="btn btn-success">
                                                                Bayar
                                                        </button>
                                                        ';
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>

                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
        </div>     
    </div>
    <!-- /.end raw card -->

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<?php foreach ($tagihan as $data) : ?>
    <div class="modal fade bd-example-modal-lg" id="modal<?php echo $data['id_tagihan']; ?>" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title">Tambah Pembayaran</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <?php echo form_open_multipart('user/input_pembayaran_by_siswa');?>
                <div class="modal-body">
                <!-- Form bayar -->
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tagihan Bulan</label>
                        <input type="text" class="form-control" value="<?= $data['bulan_bayar']; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Harus dibayar</label>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                            <span class="input-group-text">Rp.</span>
                            </div>
                            <input type="text" class="form-control" id="nominal" placeholder="Masukkan Nominal">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Bukti Pembayaran</label>

                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="berkas" id="validatedCustomFile" required>
                            <label class="custom-file-label" for="validatedCustomFile">Masukkan Bukti Pembayaran</label>
                        </div>

                        <input type="hidden" value="<?php echo $data['id_tagihan']; ?>" name="id_tagihan">
                        <input type="hidden" value="<?php echo $data['id_siswa']; ?>" name="id_siswa">
                    </div>            
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-success" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-success" value="Bayar">
                </div>
            <?php echo form_close();?>
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<?php endforeach ?>