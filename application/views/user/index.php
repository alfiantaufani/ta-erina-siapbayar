<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-primary"><?= $title; ?></h1>

    <div class="row">
        <div class="col-lg-6">
            <?= $this->session->flashdata('message'); ?>
        </div>
    </div>

    <!-- row untuk jadi satu baris card -->
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="text-center">
                                <img class="img-fluid px-3 px-sm-4 mb-2" style="width: 25rem;"
                                    src="<?php echo base_url('assets/img/undraw_posting_photo.svg'); ?>" alt="...">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <h3><b>Selamat datang di AHE Bimbel Morosunggingan.</b></h3>
                            <p>Lembaga bimbingan belajar Anak Hebat (AHE ) Morosunggingan memberikan solusi masalah orang tua tersebut untuk keperluan pendidikan anaknya.</p>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
        </div>

    </div>
    <!-- /.end raw card -->

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->