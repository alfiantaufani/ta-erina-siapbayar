<?php error_reporting(0); 
date_default_timezone_set('Asia/Jakarta');
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>

  <!-- Font Awesome -->
  <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fontawesome-free/css/all.min.css"> -->
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sb-admin-2.min.css">
</head>
<body>
<div class="wrapper">
	<section class="invoice">
		  	<div class="">
				    <div class="">
					    <div class="">
							<img src="<?= base_url('assets/'); ?>logo/siap-bayar-top.png" alt="" style="float: left; width: 85px">
				           	<div style="float: right; margin-left: 0px; top: 0px; text-align: center">
				           		<div>
					          		<span style="font-size: 25px; font-weight: bold;">
									  Bimbingan Belajar Ahe Morosunggingan
									</span><br>
						        	Alamat Jl. Kencana No.2 Morosunggingan Peterongan Jombang<br>
						        	No. Telp 0816638949 | Email ahe.morosunggingan@gmail.com
						        </div>
				           	</div>
					    </div> 
				    </div>
		    	<div class="row">
					<div class="col-md-12 mb-2">
				    	<hr>
				    </div>

				    <div class="col-sm-12 mb-4">
				    	<h5 class="page-header text-center">
				          	BUKTI PEMBAYARAN
				        </h5>
				    </div>
			    </div>
			    <?php
			      $no = 1;
			      
			    ?>
			    <div class="row">
			    	<div class="col-sm-12">
				        <div class="table-responsive">
				          <table class="table">
				            <tr>
				              <th style="width:15%">Nama</th>
				              <td>: <?php echo $tampil_transaksi['tagihan_id']; ?></td>
				            </tr>
				            <tr>
				              <th>Email</th>
				              <td>: <?php echo $tampil_transaksi['email']; ?></td>
				            </tr>
				            <tr>
				              <th>Paket Kelas </th>
				              <td>: <?php// echo $tampil_transaksi['nama']; ?></td>
				            </tr>
				            <tr>
				              <th>Jenis Kelamin</th>
				              <td>: <?php echo $tampil_transaksi['jenis_kelamin']; ?></td>
				            </tr>
				          </table>
				    	</div>
				        <p class="text-muted well well-sm shadow-none">Telah melakukan pembayaran sebagai berikut :</p>
				    </div>
			    	<div class="col-md-12 table-responsive">
				        <table class="table table-striped">
				          <thead>
					          <tr>
					            <th>No</th>
					            <th>Id Transaksi</th>
					            <th>Jenis Pembayaran</th>
					            <th>Tanggal</th>
					            <th>Jumlah Bayar</th>
					          </tr>
				          </thead>
				          <tbody>
					          <tr>
					            <td><?php echo $no++; ?></td>
					            <td><?php echo $tampil_transaksi['id']; ?></td>
					            <!-- <td><?php echo $tampil_transaksi['jenis_pembayaran']; ?></td>
					            <td><?php echo $tampil_transaksi['tgl_pembayaran']; ?></td>
					            <td>Rp. <?php echo number_format($tampil_transaksi['jumlah_bayar']);  ?></td> -->
					          </tr>
					          <tr>
					          	<td></td>
					          	<td></td>
					          	<td>Jumlah</td>
					          	<td></td>
					          	<td>Rp. <?php// echo number_format($tampil_transaksi['jumlah_bayar']); ?>
					          	</td>
					          </tr>
				          </tbody>
				        </table>
				    </div>
			    </div>
			</div>
	</section>
</div>
<br>
<p style="float: right; display: inherit; margin-left: 510px;">
	<p>Jombang, <?php echo date('d-m-yy'); ?></p>
	<p>Mengetahui,</p>
	<p style="margin-top: 50px;">
	  <?php echo $tampil_transaksi['nama_petugas']; ?>
	</p>
</p>	
