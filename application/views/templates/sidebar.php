<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('user'); ?>">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-user-graduate"></i>
        </div>
        <div class="sidebar-brand-text mx-3">AHE-<small><em>Bimbel</em></small></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">
        <div class="sidebar-heading">


        </div>
                <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
                    <a class="nav-link pb-0" href="<?php echo base_url('user') ?>">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Home</span>
                    </a>
                </li>
                <li class="nav-item <?php echo $this->uri->segment(2) == 'pembayaran' ? 'active': '' ?>">
                    <a class="nav-link pb-0" href="<?php echo base_url('user/pembayaran') ?>">
                        <i class="fas fa-fw fa-dollar-sign"></i>
                        <span>Pembayaran</span>
                    </a>
                </li>
                <li class="nav-item <?php echo $this->uri->segment(2) == 'riwayat_pembayaran' ? 'active': '' ?>">
                    <a class="nav-link pb-0" href="<?php echo base_url('riwayatuser/riwayat_pembayaran') ?>">
                        <i class="fas fa-fw fa-history"></i>
                        <span>Riwayat Pembayaran</span>
                    </a>
                </li>
                <li class="nav-item <?php echo $this->uri->segment(2) == 'profil_siswa' ? 'active': '' ?>">
                    <a class="nav-link pb-0" href="<?php echo base_url('user/profil_siswa') ?>">
                        <i class="fas fa-fw fa-user"></i>
                        <span>Profil Siswa</span>
                    </a>
                </li>
            <!-- Divider -->
            <hr class="sidebar-divider mt-3">

        <!-- Nav Item - Charts -->
        <li class="nav-item">
            <a class="nav-link btn-logout" href="<?= base_url('auth/logout'); ?>" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-fw fa-power-off"></i>
                <span>Logout</span>
            </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

</ul>
<!-- End of Sidebar -->