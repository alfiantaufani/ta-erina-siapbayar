<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>

    <!-- menampilkan pesan -->
    <div class="row">
        <div class="col-12">
            <?= $this->session->flashdata('message'); ?>
            <?php $this->session->set_userdata('referred_from', current_url()); ?>
        </div>
    </div>

    <!-- row untuk jadi satu baris card -->
    
    <!-- card data Iuran -->
    <div class="row">
        <?php  if(($this->uri->segment(2) === "cari_tagihan_siswa")){ ?>

        <div class="col-md-12">
            <div class="card shadow-lg mb-3">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Data Siswa</h4>
                </div>
                <div class="card-body">
                    <!-- Tampil data siswa -->
                    <?php 
                        foreach ($data_siswa as $data) : 
                    ?>
                    <div class="table-responsive">
                        <table class="table ">
                            <tbody>
                                <tr width="100%">
                                    <td width="20%" rowspan="5" align="center">
                                        <?php 
                                            $foto = $data['img_siswa'];

                                            if (empty($foto)) {
                                                echo '<img src="'.base_url().'assets/img/profile.jpg" width="100">';
                                            }else{
                                                echo ' <img src="'.base_url().'/assets/img/profil/'.$foto.'" width="100">';
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Nama</b></td>
                                    <td>
                                        : <?php echo $data['nama_siswa']; ?>
                                    </td>
                                    <td><b>	Jenis Kelamin</b></td>
                                    <td>
                                        : <?php echo $data['jenis_kelamin']; ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>Paket Kelas</b></td>
                                    <td>
                                        : <?php echo $data['nama']; ?>
                                    </td>
                                    <td><b>Email</b></td>
                                    <td>
                                        : <?php echo $data['email']; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card shadow-lg mb-3">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Daftar Tagihan Siswa</h4>
                </div>
                <div class="card-body">          
                        <!-- tabel tagihan -->
                        <div class="table-responsive mt-4">
                            <table class="table table-bordered" id="tableIuran">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tagihan Bulan</th>
                                        <th>Tahun</th>
                                        <th>Harus Dibayar</th>
                                        <th>Sudah Dibayar</th>
                                        <th>Kekurangan</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 1;
                                        foreach ($tampil_tagihan as $tagihan) : 
                                    ?>
                                    <tr>
                                        <td><?php echo $no++;?></td>
                                        <td><?= $tagihan['bulan_bayar'];?></td>
                                        <td><?= $tagihan['tahun'];?></td>
                                        <td>
                                            <?php 
                                                $angka = $tagihan['besaran'];
                                                $rupiah = "Rp. " . number_format($angka, 2, ',', '.');
                                                echo $rupiah;
                                            ?>
                                        </td>
                                        <td>
                                            <?php 
                                                $angka = $tagihan['sudah_dibayar'];
                                                $rupiah = "Rp. " . number_format($angka, 2, ',', '.');
                                                echo $rupiah;
                                            ?>
                                        </td>
                                        <td>
                                            <?php //kekurangan
                                                $besaran = $tagihan['besaran'];
                                                $sudah_dibayar = $tagihan['sudah_dibayar'];
                                                $hasil = $besaran - $sudah_dibayar;
                                                $rupiah = "Rp. " . number_format($hasil, 2, ',', '.');
                                                echo $rupiah;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $besaran = $tagihan['besaran'];
                                                $sudah_dibayar = $tagihan['sudah_dibayar'];
                                                if ($sudah_dibayar == $besaran) {
                                                    echo '<h5><span class="badge badge-info">Lunas</span></h5>';
                                                }else{
                                                    echo '
                                                    <button type="button" data-toggle="modal" data-target="#modal'.$tagihan['id_tagihan'].'" class="btn btn-success">
                                                            Edit
                                                    </button>
                                                    ';
                                                }
                                            ?>
                                        
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card shadow-lg mb-3">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Transaksi Pending</h4>
                </div>
                <div class="card-body">
                        <!-- tabel pending -->
                    <div class="table-responsive mt-4">
                        <table class="table table-bordered" id="tableKelas">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tagihan Bulan</th>
                                    <th>Tahun</th>
                                    <th>Harus Dibayar</th>
                                    <th>Bukti Bayar</th>
                                    <th>Status</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1;
                                    foreach ($tampil_tagihan_pending as $tagihan) : 
                                ?>
                                <tr>
                                    <td><?php echo $no++;?></td>
                                    <td><?= $tagihan['bulan_bayar'];?></td>
                                    <td><?= $tagihan['tahun'];?></td>
                                    <td>
                                        <?php 
                                            $angka = $tagihan['besaran'];
                                            $rupiah = "Rp. " . number_format($angka, 2, ',', '.');
                                            echo $rupiah;
                                        ?>
                                    </td>
                                    <td>
                                       <a href="<?= base_url('assets/img/img_bukti')?>/<?=$tagihan['bukti_transfer'];?>" target="_blank">
                                            <img src="<?= base_url('assets/img/img_bukti')?>/<?=$tagihan['bukti_transfer'];?>" alt="" width="100">
                                       </a>
                                    </td>
                                    <td><h5><span class="badge badge-warning"><?=$tagihan['status']?></span></h5></td>
                                    <td>
                                        <?php
                                            if (!$tagihan['status'] == 'pending') {
                                                echo '<h5><span class="badge badge-info">'.$tagihan['status'].'</span></h5>';
                                            }else{
                                                echo '
                                                <a href="'.base_url('admin/konfirmasi').'/'.$tagihan['id_tagihan'].'" class="btn btn-success">
                                                        Konfirmasi
                                                </a>
                                                ';
                                            }
                                        ?>
                                       
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card shadow-lg mb-3">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Riwayat Bukti Transfer</h4>
                </div>
                <div class="card-body">                                      
                    <!-- Tabel riwayat transfer -->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tableSemester">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tagihan Bulan</th>
                                    <th>Tahun</th>
                                    <th>Harus Dibayar</th>
                                    <th>Bukti Bayar</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1;
                                    foreach ($tampil_tagihan_lunas as $tagihan) : 
                                ?>
                                <tr>
                                    <td><?php echo $no++;?></td>
                                    <td><?= $tagihan['bulan_bayar'];?></td>
                                    <td><?= $tagihan['tahun'];?></td>
                                    <td>
                                        <?php 
                                            $angka = $tagihan['besaran'];
                                            $rupiah = "Rp. " . number_format($angka, 2, ',', '.');
                                            echo $rupiah;
                                        ?>
                                    </td>
                                    <td>
                                    <a href="<?= base_url('assets/img/img_bukti')?>/<?=$tagihan['bukti_transfer'];?>" target="_blank">
                                            <img src="<?= base_url('assets/img/img_bukti')?>/<?=$tagihan['bukti_transfer'];?>" alt="" width="100">
                                    </a>
                                    </td>
                                    <td><h5><span class="badge badge-info"><?=$tagihan['status']?></span></h5></td>
                                    
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card shadow-lg mb-3">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Riwayat Transaksi Pembayaran Siswa</h4>
                </div>
                <div class="card-body">                                      
                    <!-- Tabel riwayat transfer -->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tableSemester">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Pembayaran</th>
                                    <th>Jenis Pembayaran</th>
                                    <th>Tanggal</th>
                                    <th>Nominal Bayar</th>
                                    <th>Penerima</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $no = 1;
                                    foreach ($tampil_transaksi as $transaksi) : 
                                ?>
                                <tr>
                                    <td><?php echo $no++;?></td>
                                    <td><?= $transaksi['id']; ?><?= preg_replace('/-/', '', $transaksi['tgl_bayar']) ; ?></td>
                                    <td><?= $transaksi['bulan_bayar'];?></td>
                                    <td>
                                        <?= date('d F Y', strtotime($transaksi['tgl_bayar'])); ?>
                                    </td>
                                    <td>
                                        <?php 
                                            $angka = $transaksi['jmlh_bayar'];
                                            $rupiah = "Rp. " . number_format($angka, 2, ',', '.');
                                            echo $rupiah;
                                        ?>
                                    </td>
                                    <td><?= $transaksi['nama_petugas']; ?></td>
                                    <td>
                                        <a href="#detailModal<?= $transaksi['id']; ?>" data-toggle="modal" class="btn btn-info">
                                                <i class="fas fa-fw fa-eye fa-sm"></i> Lihat Bukti
                                        </a>
                                    </td>
                                    
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>    
        
        <?php }else{?>
            <!-- Tabel Daftar Siswa -->
            <div class="col-md-12">
                <div class="card shadow-lg mb-3">
                    <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary">Daftar Siswa</h4>
                    </div>
                    <div class="card-body">      
                        
                            <div class="table-responsive">
                                <table class="table table-bordered" id="tableSemester">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Nama Siswa</th>
                                            <th scope="col">Jenis Kelamin</th>
                                            <th scope="col">Paket Kelas</th>
                                            <th scope="col">Opsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach ($tampil_tagihan as $kr) : ?>
                                            <tr>
                                                <td><?= $no++; ?></td>
                                                <td>
                                                <?= $kr['nama_siswa']; ?>
                                                </td>
                                                <td><?= $kr['jenis_kelamin']; ?></td>
                                                <td><?= $kr['nama']; ?></td>
                                                <td class="text-center">
                                                    <a href="<?= base_url('admin/cari_tagihan_siswa'); ?>/<?=$kr['id_siswa']?>" class="btn btn-info mr-1"><i class="fas fa-dollar-sign fa-sm"></i> Tampil Tagihan</a>
                                                    
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <!-- /.card data Iuran -->

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<?php  if(($this->uri->segment(2) === "cari_tagihan_siswa")){ ?>
    <?php foreach ($tampil_tagihan as $data) : ?>
    <div class="modal fade bd-example-modal-lg" id="modal<?php echo $data['id_tagihan']; ?>" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title">Tambah Pembayaran</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            </div>
            <?php echo form_open_multipart('admin/input_pembayaran');?>
                <div class="modal-body">
                <!-- Form bayar -->
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tagihan Bulan</label>
                        <input type="text" class="form-control" value="<?php echo $data['bulan_bayar']; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Jumlah Bayar</label>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                            <span class="input-group-text">Rp.</span>
                            </div>
                            <input type="hidden" name="sudah_dibayar" value="<?php echo $data['sudah_dibayar']; ?>">
                            <input type="number" class="form-control" name="jumlah_bayar" placeholder="Masukkan Nominal Pembayaran" required="" autofocus="AuToFoCuS">
                        </div>

                        <input type="hidden" value="<?php echo $data['id_tagihan']; ?>" name="id_tagihan">
                        <input type="hidden" value="<?php echo $data['id_siswa']; ?>" name="id_siswa">
                    </div>            
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-success" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-success" value="Bayar">
                </div>
            <?php echo form_close();?>
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <?php endforeach; ?>

    <?php foreach ($tampil_transaksi as $t) : ?>
        <div class="modal fade" id="detailModal<?= $t['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Detail Trasnsaksi Siswa</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="invoice-title">
                                <h4 class="lead text-gray-800 d-none d-lg-block text-center">
                                    <img src="<?= base_url('assets/'); ?>logo/siap-bayar-top.png" alt="logo-image" class="img-circle">Bimbingan Belajar Ahe Morosunggingan
                                </h4>
                                <h2><center><b>Bukti Pembayaran</b></center></h2>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <address>
                                    <strong>Nama Siswa :</strong><br>
                                    <?= $t['nama_siswa']; ?>
                                    </address>
                                    <address>
                                        <strong>Paket Kelas :</strong><br>
                                        <?= $t['nama']; ?>
                                    </address>
                                </div>
                                <div class="col-md-6 text-right">
                                    <address>
                                    <strong>Kode Pembayaran :</strong><br>
                                    <?= $t['id']; ?><?= preg_replace('/-/', '', $t['tgl_bayar']) ; ?>
                                    </address>

                                    <address>
                                        <strong>Tanggal Pembayaran:</strong><br>
                                        <?= date('d F Y', strtotime($t['tgl_bayar'])); ?>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title"><strong>Untuk Pembayaran :</strong></h5>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-condensed">
                                            <thead>
                                                <tr>
                                                    <td><strong>Jenis Pembayaran Bulan</strong></td>
                                                    <td class="text-center"><strong></strong></td>
                                                    <td class="text-right"><strong>Jumlah Bayar</strong></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                <tr>
                                                    <td><?= $t['bulan_bayar']; ?></td>
                                                    <td class="text-center"></td>
                                                    <td class="text-right">
                                                        <?php 
                                                            $angka = $t['jmlh_bayar'];
                                                            $rupiah = "Rp " . number_format($angka, 2, ',', '.');
                                                            echo $rupiah;
                                                        ?>
                                                    </td>
                                                    
                                                </tr>
                                                
                                                <tr>
                                                    <td class="no-line"></td>
                                                    <td class="no-line text-right"><strong>Total</strong></td>
                                                    <td class="no-line text-right">
                                                    <?php 
                                                            $angka = $t['jmlh_bayar'];
                                                            $rupiah = "Rp " . number_format($angka, 2, ',', '.');
                                                            echo $rupiah;
                                                        ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-right">
                                <address>
                                    Jombang, <?= date('d F Y', strtotime($t['tgl_bayar'])); ?><br>
                                    Diterima Oleh<br><br><br>
                                    <?= $t['nama_petugas']; ?>
                                </address>
                            </div>
                    </div>

                    </div>

                    <div class="modal-footer">
                        <a href="<?= base_url('admin/print_transaksi/'.$t['id'].'')?>" target="_blank" class="btn btn-info">
                            <i class="fas fa-fw fa-print fa-sm"></i> Print
                        </a>
                        <button class="btn btn-outline-info" data-dismiss="modal" aria-label="Close">
                        Tutup
                        </button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

<?php } ?>