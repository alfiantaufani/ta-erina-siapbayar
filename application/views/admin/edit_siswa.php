<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>

    <!-- menampilkan pesan -->
    <div class="row">
        <div class="col-12">
            <?= $this->session->flashdata('message'); ?>
        </div>
    </div>

    <!-- row untuk jadi satu baris card -->

    <!-- card data Iuran -->
    <div class="row">
        <div class="col">
            <div class="card shadow-lg mb-3">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary"><?= $title; ?></h4>
                    <!-- <a class="btn btn-primary shadow" href="#"><i class="fas fa-coins pr-2 fa-sm text-white-50"></i> Input Data Pembayaran</a> -->
                </div>
                <div class="card-body">
                    <form class="user" method="post" action="<?= base_url('admin/update_siswa/'); ?>">
                        <div class="form-group">
                            <label for="">Nama Siswa</label>
                            <input type="hidden" class="form-control" name="id" value="<?= $edit_siswa['id']; ?>" required>
                            <input type="text" class="form-control" id="name" name="nama_siswa" value="<?= $edit_siswa['nama_siswa']; ?>" required>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                            <label for="">NIK Siswa</label>
                            <input type="number" class="form-control" id="name" name="nik_siswa" placeholder="Masukkan NIK Siswa" value="<?= $edit_siswa['nik']; ?>" required>
                            </div>
                            <div class="col-sm-6">
                            <label for="">NOK Siswa</label>
                            <input type="number" class="form-control" id="name" name="nok_siswa" placeholder="Masukkan NOK Siswa" value="<?= $edit_siswa['nok']; ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Jenis Kelamin</label>
                            <select name="jenis_kelamin" id="" class="form-control"  required>
                                <option selected hidden value="<?= $edit_siswa['jenis_kelamin']; ?>"><?= $edit_siswa['jenis_kelamin']; ?></option>
                                <option value="Laki-laki">Laki-laki</option>
                                <option value="Perempuan">Perempaun</option>
                            </select>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <div class="form-group">
                                    <label for="">Nama Ayah</label>
                                    <input type="text" class="form-control" id="name" name="nama_ayah" placeholder="Masukkan Ayah" value="<?= $edit_siswa['nama_ayah']; ?>" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="">Nama Ibu</label>
                                    <input type="text" class="form-control" id="name" name="nama_ibu" placeholder="Masukkan Ibu" value="<?= $edit_siswa['nama_ibu']; ?>" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="">No. Telepon Orang Tua</label>
                            <input type="text" class="form-control" id="name" name="no_hp_ortu" placeholder="Masukkan No. Telepon yang bisa dihubungi" value="<?= $edit_siswa['no_hp_ortu']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="">Alamat Orang Tua</label>
                            <textarea class="form-control" name="alamat_ortu" id="" cols="30" rows="2" placeholder="Masukkan Alamat Orang Tua" value=""><?= $edit_siswa['alamat_ortu']; ?></textarea>
                        </div>
                        <!--
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Masukkan Email" value="<?= $edit_siswa['email']; ?>">
                        </div> -->
                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            Simpan
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card data Iuran -->

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


