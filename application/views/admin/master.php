<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>

    <!-- menampilkan pesan -->
    <div class="row">
        <div class="col-12">
            <?= $this->session->flashdata('message'); ?>
        </div>
    </div>

    <!-- row untuk jadi satu baris card -->

    <?php if (($this->uri->segment(2) === "pilih_pembayaran")) { ?>
        <div class="row">
            <div class="col">
                <div class="card shadow-lg mb-3">
                    <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary">Form Data Pembayaran</h4>
                    </div>
                    <div class="card-body">
                    <form method="post" action="<?= base_url('tagihan/insert_pembayaran'); ?>">
                        <div class="row">
                            <div class="col-lg">
                                <?php foreach ($tampil_siswa_sesuai_paket as $datane ) : ?>
                                    <input type="hidden" value="<?= $datane['id_siswa']; ?>" name="id_siswa[]">
                                <?php endforeach; ?>
                                <input type="hidden" value="<?= $paket_kelas; ?>" name="paket_kelas">
                                <div class="form-group">
                                    <label for="tahun">Tahun</label>
                                    <input type="number" class="form-control" id="tahun" name="tahun" onKeyPress="if(this.value.length==4) return false;" placeholder="Masukkan Tahun, contoh : 2021" autofocus="AuToFoCuS">
                                </div>
                                <div class="form-group">
                                    <label for="tahun">Bulan</label>
                                    <input type="text" class="form-control" id="tahun" name="bulan" placeholder="Masukkan Bulan, contoh : Juli">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Besaran</label>

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text">Rp.</span>
                                        </div>
                                        <input type="number" class="form-control" name="besaran" placeholder="Masukkan Nominal Besaran, contoh : 15000" required="">
                                    </div>
                                </div> 
                                
                                <div class="form-group float-right">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    <?php }else{ ?>

    <!-- card data pembayaran -->
    <div class="row">
        <div class="col">
            <div class="card shadow-lg mb-3">
                <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Data Master Pembayaran</h4>
                    <a class="btn btn-primary shadow" href="#tambahPembayaran" data-toggle="modal"><i class="fas fa-coins pr-2 fa-sm text-white-50"></i> Input Data Pembayaran</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tableIuran">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Paket Kelas</th>
                                    <th scope="col">Tahun</th>
                                    <th scope="col">Bulan</th>
                                    <th scope="col">Besaran Rp</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach ($pembayaran as $i) : ?>
                                    <tr>
                                        <td><?= $no++; ?></td> 
                                        <td><?= $i['nama']; ?></td>
                                        <td><?= $i['tahun']; ?></td>
                                        <td><?= $i['bulan_bayar']; ?></td>
                                        <td>
                                            <?php $angka = $i['besaran'];
                                            $rupiah = "Rp " . number_format($angka, 2, ',', '.');
                                            echo $rupiah;
                                            ?>
                                        </td>
                                        <td class="text-center">
                                            <a href="#editPembayaran<?= $i['id_pembayaran']; ?>" data-toggle="modal" class="btn btn-info mr-1"><i class="fas fa-edit fa-sm"></i> Edit</a>
                                            <a href="<?=base_url('admin/delete_pembayaran')?>/<?= $i['id_pembayaran']; ?>" class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data ini?');"><i class="fas fa-trash-alt fa-sm"></i> Hapus</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card data pembayaran -->

    <!-- card data Kelas -->
    <div class="row">
        <div class="col">
            <div class="card shadow-lg mb-3">
            <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">Data Master Paket Kelas</h4>
                    <a class="btn btn-primary shadow" href="#tambahPaket" data-toggle="modal"><i class="fas fa-coins pr-2 fa-sm text-white-50"></i> Input Data Paket Kelas</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tableKelas">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Nama Kelas</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach ($paket_kelas as $k) : ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $k['nama']; ?></td>
                                        <td class="text-center">
                                            <a href="#editKelas<?= $k['id']; ?>" data-toggle="modal" class="btn btn-info mr-1"><i class="fas fa-edit fa-sm"></i> Edit</a>
                                            <!-- <a href="#" class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data ini?');">
                                                <i class="fas fa-trash-alt fa-sm"></i>
                                                Hapus
                                            </a> -->
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card data kelas -->
                                
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<!-- tambah Pembayaran Modal-->
<div class="modal fade" id="tambahPembayaran" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Data Pembayaran</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">

                <form method="post" action="<?= base_url('tagihan/pilih_pembayaran'); ?>">
                    <div class="row">
                        <div class="col-lg">
                            <div class="form-group">
                            <label for="">Pilih Paket Kelas</label>
                            <select name="paket_kelas" id="" class="form-control" required>
                                <option disabled selected hidden>-- Pilih Paket Kelas --</option>
                                <?php foreach ($paket_kelas as $data) :?>
                                    <option value="<?= $data['id'] ?>"><?= $data['nama'] ?></option>
                                <?php endforeach ?>
                            </select>
                            </div>
                            
                            <div class="form-group float-right">
                                <button class="btn btn-outline-primary ml-2" role="button" data-dismiss="modal" aria-label="Close">Batal</button> 
                                <button type="submit" class="btn btn-primary">Next</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- akhir form input -->

            </div>
        </div>
    </div>
</div>
<!-- /.akhir tambah pembayaran Modal -->

<!-- tambah Pembayaran Modal-->
<div class="modal fade" id="tambahPaket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Paket Kelas</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">

                <form method="post" action="<?= base_url('admin/input_paket'); ?>">
                    <div class="row">
                        <div class="col-lg">
                            <div class="form-group">
                                <label for="">Nama Paket Kelas</label>
                                <input type="text" class="form-control" name="nama" placeholder="Masukkan nama paket kelas">
                            </div>
                            
                            <div class="form-group float-right">
                                <button class="btn btn-outline-primary ml-2" role="button" data-dismiss="modal" aria-label="Close">Batal</button> 
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- akhir form input -->

            </div>
        </div>
    </div>
</div>
<!-- /.akhir tambah pembayaran Modal -->

<!-- Edit Pembayaran -->
<?php foreach ($pembayaran as $i) : ?>
    <div class="modal fade" id="editPembayaran<?= $i['id_pembayaran']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data Pembayaran</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">

                    <form method="post" action="<?= base_url('admin/update_pembayaran'); ?>">
                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <input type="hidden" value="<?=$i['id_pembayaran'];?>" name="id_pembayaran">
                                    <label for="">Pilih Paket Kelas</label>
                                    <select name="paket_kelas" id="" class="form-control" required>
                                        
                                        <option selected hidden value="<?=$i['id_kelas'];?>"><?=$i['nama'];?></option>

                                        <?php foreach ($paket_kelas as $data) :?>
                                            <option value="<?= $data['id'] ?>"><?= $data['nama'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Tahun</label>
                                    <input type="text" class="form-control" name="tahun" placeholder="Masukkan nama paket kelas" value="<?= $i['tahun']; ?>">
                                </div>

                                <div class="form-group">
                                    <label for="">Bulan</label>
                                    <input type="text" class="form-control" name="bulan" placeholder="Masukkan nama paket kelas" value="<?= $i['bulan_bayar']; ?>">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Besaran</label>

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text">Rp.</span>
                                        </div>
                                        <input type="number" class="form-control" name="besaran" placeholder="Masukkan Nominal Besaran, contoh : 15000" required="" value="<?=$i['besaran'];?>">
                                    </div>
                                </div> 
                                
                                <div class="form-group float-right">
                                    <button class="btn btn-outline-primary ml-2" role="button" data-dismiss="modal" aria-label="Close">Batal</button> 
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- akhir form input -->

                </div>
            </div>
        </div>
    </div>
<?php endforeach ?>
<!-- End Edit pembayaran -->

<!-- Edit Paket Kelas Modal-->
<?php foreach ($paket_kelas as $k) : ?>
<div class="modal fade" id="editKelas<?= $k['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Paket Kelas</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">

                <form method="post" action="<?= base_url('admin/update_paket'); ?>">
                    <div class="row">
                        <div class="col-lg">
                            <div class="form-group">
                                <label for="">Nama Paket Kelas</label>
                                <input type="hidden" name="id_paket" value="<?=$k['id'];?>">
                                <input type="text" class="form-control" name="nama" placeholder="Masukkan nama paket kelas" value="<?= $k['nama']; ?>">
                            </div>
                            
                            <div class="form-group float-right">
                                <button class="btn btn-outline-primary ml-2" role="button" data-dismiss="modal" aria-label="Close">Batal</button> 
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- akhir form input -->

            </div>
        </div>
    </div>
</div>
<?php endforeach ?>
<!-- /.akhir Edit Paket Kelas Modal -->

<?php } ?>