<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('user'); ?>">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-user-graduate"></i>
        </div>
        <div class="sidebar-brand-text mx-3">AHE-<small><em>Bimbel</em></small></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">
        <div class="sidebar-heading">


        </div>
                <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
                    <a class="nav-link pb-0" href="<?php echo base_url('admin') ?>">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Home</span>
                    </a>
                </li>
                <li class="nav-item <?php echo $this->uri->segment(2) == 'master' ? 'active': '' ?>">
                    <a class="nav-link pb-0" href="<?php echo base_url('admin/master') ?>">
                        <i class="fas fa-fw fa-database"></i>
                        <span>Data Master</span>
                    </a>
                </li>
                <li class="nav-item <?php echo $this->uri->segment(2) == 'data_master_siswa' ? 'active': '' ?>">
                    <a class="nav-link pb-0" href="<?php echo base_url('admin/data_master_siswa') ?>">
                        <i class="fas fa-fw fa-users"></i>
                        <span>Data Master Siswa</span>
                    </a>
                </li>
                <li class="nav-item <?php echo $this->uri->segment(2) == 'transaksi' ? 'active': '' ?>">
                    <a class="nav-link pb-0" href="<?php echo base_url('admin/transaksi') ?>">
                        <i class="fas fa-fw fa-dollar-sign"></i>
                        <span>Transaksi</span>
                    </a>
                </li>
            <!-- Divider -->
            <hr class="sidebar-divider mt-3">

        <!-- Nav Item - Charts -->
        <li class="nav-item">
            <a class="nav-link btn-logout" href="<?= base_url('auth/logout'); ?>" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-fw fa-power-off"></i>
                <span>Logout</span>
            </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

</ul>
<!-- End of Sidebar -->