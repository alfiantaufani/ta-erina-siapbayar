<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>

    <!-- menampilkan pesan -->
    <div class="row">
        <div class="col-12">
            <?= $this->session->flashdata('message'); ?>
        </div>
    </div>

    <!-- row untuk jadi satu baris card -->
    <?php  if(($this->uri->segment(2) === "edit_siswa")){ ?>
        <!-- Card Edit siswa -->
        <div class="row">
            <div class="col">
                <div class="card shadow-lg mb-3">
                    <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary"><?= $title; ?></h4>
                    </div>
                    <div class="card-body">
                        <?php echo form_open_multipart('admin/update_siswa');?>
                            <div class="row">
                                <div class="col-lg">
                                    <div class="form-group">
                                        <label for="nama_siswa">Nama Siswa</label>
                                        <input type="text" class="form-control" name="nama_siswa" id="nama_siswa" value="<?= $data_siswa['nama_siswa']; ?>" readonly>

                                        <input type="hidden" name="id_siswa" value="<?=$data_siswa['id_siswa'];?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="nik">Email</label>
                                        <input type="text" class="form-control" name="email" id="email" value="<?= $data_siswa['email']; ?>" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nik">Nomor Induk Kependudakan (NIK)</label>
                                        <input type="text" class="form-control" name="nik" id="nik" value="<?= $data_siswa['nik']; ?>" readonly>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="nama_siswa">Jenis Kelamin</label>
                                        <select name="jenis_kelamin" id="jk" class="form-control" disabled>
                                            <option value="<?= $data_siswa['jenis_kelamin']; ?>" selected hidden><?= $data_siswa['jenis_kelamin']; ?></option>
                                            <option value="Perempuan">Perempuan</option>
                                            <option value="Laki-laki">Laki-laki</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg">
                                    <div class="form-group">
                                        <label for="kelas">Paket Kelas</label>
                                        <select name="paket_kelas" id="paket_kelas" class="form-control" disabled>
                                            <option value="<?= $data_siswa['id'] ?>" selected hidden><?= $data_siswa['nama'] ?></option>
                                            <?php foreach ($paket_kelas as $data) :?>
                                                <option value="<?= $data['id'] ?>"><?= $data['nama'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_ayah">Nama Ayah</label>
                                        <input type="text" class="form-control" name="nama_ayah" value="<?= $data_siswa['nama_ayah']; ?>" id="nama_ayah" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_ibu">Nama Ibu</label>
                                        <input type="text" class="form-control" name="nama_ibu" id="nama_ibu" value="<?= $data_siswa['nama_ibu']; ?>" readonly>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="alamat_ortu">Alamat Lengkap Orang Tua</label>
                                        <textarea class="form-control" name="alamat_ortu" id="alamat_ortu" rows="2" readonly><?= $data_siswa['alamat_ortu']; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group float-left">
                                        <button type="button" class="btn btn-primary shadow-lg" id="edt" onclick="edit()">Edit Data Siswa</button>
                                        <button type="submit" class="btn btn-primary shadow-lg" id="simpan" style="display:none;">Simpan</button>

                                        <button type="reset" class="btn btn-outline-primary ml-2" id="batal" role="button" style="display:none;" onclick="btl()">Batal</button>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close();?>                        
                    </div>
                </div>
            </div>
        </div>
    <?php }else{ ?>
        <!-- card data Siswa -->
        <div class="row">
            <div class="col">
                <div class="card shadow-lg mb-3">
                    <div class="card-header py-3 d-sm-flex align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary"><?= $title; ?></h4>
                        <!-- <a class="btn btn-primary shadow" href="#"><i class="fas fa-coins pr-2 fa-sm text-white-50"></i> Input Data Pembayaran</a> -->
                    </div>
                    <div class="card-body">
                    
                        <div class="table-responsive">
                            <table class="table table-bordered" id="tableIuran">
                                <thead>
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama Siswa</th>
                                        <th scope="col">Jenis Kelamin</th>
                                        <th scope="col">Paket Kelas</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Tanggal Daftar</th>
                                        <th scope="col">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no=1; foreach ($data_siswa as $i) : ?>
                                        <tr>
                                            <td><?= $no++; ?></td> 
                                            <td><?= $i['nama_siswa']; ?></td>
                                            <td><?= $i['jenis_kelamin']; ?></td>
                                            <td><?= $i['nama']; ?></td>
                                            <td><?= $i['email']; ?></td>
                                            <td><?php echo format_indo($i['created_at']);?></td>
                                            <td class="text-center">
                                                <a href="#detail<?= $i['id_siswa']; ?>" data-toggle="modal" class="btn btn-primary mr-1"><i class="fas fa-eye fa-sm"></i> Lihat</a>

                                                <a href="<?=base_url('admin/edit_siswa')?>/<?= $i['id_siswa']; ?>" class="btn btn-info mr-1"><i class="fas fa-edit fa-sm"></i> Edit</a>

                                                <a href="#" class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data ini?');"><i class="fas fa-trash-alt fa-sm"></i> Hapus</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card data Iuran -->
    <?php } ?>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<?php if(($this->uri->segment(2) === "data_master_siswa")){ ?>

    <!-- edit Iuran Modal-->
    <?php foreach ($data_siswa as $i) : ?>
        <div class="modal fade" id="detail<?= $i['id_siswa']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Data Siswa</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <?php echo form_open_multipart('admin/update_siswa');?>
                            <div class="row">
                                <div class="col-lg">
                                    <div class="form-group">
                                        <label for="nama_siswa">Nama Siswa</label>
                                        <input type="text" class="form-control" name="nama_siswa" id="nama_siswa" value="<?= $i['nama_siswa']; ?>" readonly>
                                        <input type="hidden" name="id_siswa" value="<?=$i['id_siswa'];?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="nik">Email</label>
                                        <input type="text" class="form-control" name="email" id="email" value="<?= $i['email']; ?>" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nik">Nomor Induk Kependudakan (NIK)</label>
                                        <input type="text" class="form-control" name="nik" id="nik" value="<?= $i['nik']; ?>" readonly>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="nama_siswa">Jenis Kelamin</label>
                                        <select name="jenis_kelamin" id="jk" class="form-control" disabled>
                                            <option value="<?= $i['jenis_kelamin']; ?>" disabled selected><?= $i['jenis_kelamin']; ?></option>
                                            <option value="Perempuan">Perempuan</option>
                                            <option value="Laki-laki">Laki-laki</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg">
                                    <div class="form-group">
                                        <label for="kelas">Paket Kelas</label>
                                        <select name="paket_kelas" id="paket_kelas" class="form-control" disabled>
                                            <option value="<?= $i['id'] ?>" disabled selected><?= $i['nama'] ?></option>
                                            <?php foreach ($paket_kelas as $data) :?>
                                                <option value="<?= $data['id'] ?>"><?= $data['nama'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_ayah">Nama Ayah</label>
                                        <input type="text" class="form-control" name="nama_ayah" value="<?= $i['nama_ayah']; ?>" id="nama_ayah" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_ibu">Nama Ibu</label>
                                        <input type="text" class="form-control" name="nama_ibu" id="nama_ibu" value="<?= $i['nama_ibu']; ?>" readonly>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="alamat_ortu">Alamat Lengkap Orang Tua</label>
                                        <textarea class="form-control" name="alamat_ortu" id="alamat_ortu" rows="2" readonly><?= $i['alamat_ortu']; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group float-left">
                                        <!-- <button type="button" class="btn btn-primary shadow-lg" id="edt" onclick="edit()">Edit Data Siswa</button>
                                        <button type="submit" class="btn btn-primary shadow-lg" id="simpan" style="display:none;">Simpan</button>

                                        <button type="reset" class="btn btn-outline-primary ml-2" id="batal" role="button" style="display:none;" onclick="btl()">Batal</button> -->
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close();?>
                    </div>

                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    <!-- /.akhir edit Iuran Modal -->
    

<?php } ?>
<script>
    function edit() {
        //var edit = $('#edit');
        $('#simpan').show();
        $('#batal').show();
        $('#edt').hide();

        $('#email').attr('readonly', false);
        $('#nik').attr('readonly', false);
        $('#nama_siswa').attr('readonly', false);
        
        $('#jk').removeAttr('disabled');
        $('#paket_kelas').removeAttr('disabled');
        $('#nama_ayah').attr('readonly', false);
        $('#nama_ibu').attr('readonly', false);
        $('#alamat_ortu').attr('readonly', false);
    }
    
    function btl() {
        $('#simpan').hide();
        $('#batal').hide();
        $('#edt').show();

        $('#email').attr('readonly', true);
        $('#nik').attr('readonly', true);
        $('#nama_siswa').attr('readonly', true);
        
        $('#jk').attr('disabled', 'disabled');
        $('#paket_kelas').attr('disabled', 'disabled');
        $('#nama_ayah').attr('readonly', true);
        $('#nama_ibu').attr('readonly', true);
        $('#alamat_ortu').attr('readonly', true);
    }
</script>